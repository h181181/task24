﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.timeStamp = DateTime.Now;
            return View("MyFirstView");
        }


        public IActionResult SupervisorInfo()
        {
            Supervisor per = new Supervisor { Id = 1, Name = "Per" };
            Supervisor dean = new Supervisor { Id = 2, Name = "Dean" };
            Supervisor greg = new Supervisor { Id = 3, Name = "Greg" };
            Supervisor mark = new Supervisor { Id = 4, Name = "Mark" };

            List<Supervisor> supervisors = new List<Supervisor>();
                     
            supervisors.Add(per);
            supervisors.Add(dean);
            supervisors.Add(greg);
            supervisors.Add(mark);
                          
            return View(supervisors);
        }

    }
}
